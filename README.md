### Install and run dep

- Install dep -> https://github.com/golang/dep
- Run `dep ensure`

### Run this app

```
> go run .\app
```

### Select calculator menu

```
? Select Calculator:
  > Sum 2 numbers
    Multiply 2 numbers
    Find N prime numbers
    Find N fibonacci sequence
    Quit
```

Then press `Enter`

*Note*:
- For windows using  j, k (down, up)
- For linux/macos can using arrows key

### Run test

```
> go test -v .\math
```