package main

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/manifoldco/promptui"
	mathx "gitlab.com/tonidy/golangtest/math"
)

func replaceNewlineAndSpace(s string) string {
	re := regexp.MustCompile(`\r\n|[\r\n]|\s+`)
	return re.ReplaceAllString(s, "")
}

func sum() {
	validate := func(input string) error {
		if !strings.Contains(input, ",") {
			return errors.New("Please input 2 operands. Example: 1, 2")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Input ",
		Validate: validate,
	}

	result, err := prompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}
	numbers := strings.Split(replaceNewlineAndSpace(result), ",")
	fmt.Printf("Output: %d\n", mathx.Sum(numbers))
}

func multiply() {
	validate := func(input string) error {
		if !strings.Contains(input, ",") {
			return errors.New("Please input 2 operands. Example: 1, 2")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Input ",
		Validate: validate,
	}

	result, err := prompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}
	numbers := strings.Split(replaceNewlineAndSpace(result), ",")
	fmt.Printf("Output: %d\n", mathx.Multiply(numbers))
}

func findPrime() {
	validate := func(input string) error {
		if _, err := strconv.ParseInt(input, 10, 32); err != nil {
			return errors.New("Please input number")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Input ",
		Validate: validate,
	}

	result, err := prompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}
	if number, err := strconv.Atoi(result); err == nil {
		fmt.Printf("Output: %d\n", mathx.FindPrime(number))
	}
}

func findFibonacci() {
	validate := func(input string) error {
		if _, err := strconv.ParseUint(input, 10, 64); err != nil {
			return errors.New("Please input number")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Input ",
		Validate: validate,
	}

	result, err := prompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}
	if number, err := strconv.ParseUint(result, 10, 64); err == nil {
		fmt.Printf("Output: %d\n", mathx.FindFibonacci(number))
	}
}

func main() {
	prompt := promptui.Select{
		Label: "Select Calculator",
		Items: []string{
			"Sum 2 numbers",
			"Multiply 2 numbers",
			"Find N prime numbers",
			"Find N fibonacci sequence",
			"Quit",
		},
	}

	index, _, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}

	switch index {
	case 0:
		sum()
	case 1:
		multiply()
	case 2:
		findPrime()
	case 3:
		findFibonacci()
	default:
		return
	}
}
