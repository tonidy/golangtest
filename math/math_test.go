package mathx

import (
	"math"
	"math/big"
	"reflect"
	"strconv"
	"testing"
)

// AssertEqual checks if values are equal
// Refactoring code from: https://gist.github.com/samalba/6059502#gistcomment-2710184
func AssertEqual(t *testing.T, a interface{}, b interface{}) {
	bigInt := new(big.Int)
	sliceInt := []uint64{}
	bigSliceInt := []*big.Int{}

	if reflect.TypeOf(a) == reflect.TypeOf(sliceInt) &&
		reflect.TypeOf(b) == reflect.TypeOf(sliceInt) {
		aSliceInt := a.([]uint64)
		bSliceInt := b.([]uint64)
		if reflect.DeepEqual(aSliceInt, bSliceInt) {
			return
		}
	} else if reflect.TypeOf(a) == reflect.TypeOf(bigSliceInt) &&
		reflect.TypeOf(b) == reflect.TypeOf(bigSliceInt) {
		aSliceInt := a.([]*big.Int)
		bSliceInt := b.([]*big.Int)
		if reflect.DeepEqual(aSliceInt, bSliceInt) {
			return
		}
	} else if a == b {
		return
	} else if reflect.TypeOf(a) == reflect.TypeOf(bigInt) {
		aBigInt := a.(*big.Int)
		var bBigInt *big.Int
		switch reflect.TypeOf(b) {
		case reflect.TypeOf(int(0)):
			bInt := b.(int)
			bBigInt = big.NewInt(int64(bInt))
		case reflect.TypeOf(bigInt):
			bBigInt = b.(*big.Int)
		}

		if aBigInt.Cmp(bBigInt) == 0 {
			return
		}
	}
	if reflect.TypeOf(a) != reflect.TypeOf(b) {
		t.Errorf("Received %v (type %v), expected %v (type %v)", a, reflect.TypeOf(a), b, reflect.TypeOf(b))
	} else {
		t.Errorf("Actual %v but expected %v", a, b)
	}
}

func TestSmallSum(t *testing.T) {
	actualSum := Sum([]string{"100", "123"})
	expectedSum := 223
	AssertEqual(t, actualSum, expectedSum)
}

func TestMaxInt32Sum(t *testing.T) {
	actualSum := Sum([]string{strconv.Itoa(math.MaxInt32), "1"})
	expectedSum := 2147483648
	AssertEqual(t, actualSum, expectedSum)
}

func TestMaxInt64Sum(t *testing.T) {
	actualSum := Sum([]string{strconv.Itoa(math.MaxInt64), "1"})
	expectedSum, _ := new(big.Int).SetString("9223372036854775808", 10)
	AssertEqual(t, actualSum, expectedSum)
}

func TestBigSum(t *testing.T) {
	actualSum := Sum([]string{"99999999999999999999999999999999999", "1"})
	expectedSum, _ := new(big.Int).SetString("100000000000000000000000000000000000", 10)
	AssertEqual(t, actualSum, expectedSum)
}

func TestSmallMultiply(t *testing.T) {
	actualMul := Multiply([]string{"235", "1978"})
	expectedMul := 464830
	AssertEqual(t, actualMul, expectedMul)
}

func TestMaxInt64Multiply(t *testing.T) {
	actualMul := Multiply([]string{strconv.Itoa(math.MaxInt64), "100"})
	expectedMul, _ := new(big.Int).SetString("922337203685477580700", 10)
	AssertEqual(t, actualMul, expectedMul)
}

func TestBigMultiply(t *testing.T) {
	actualMul := Multiply([]string{strconv.Itoa(math.MaxInt64), strconv.Itoa(math.MaxInt64)})
	expectedMul, _ := new(big.Int).SetString("85070591730234615847396907784232501249", 10)
	AssertEqual(t, actualMul, expectedMul)
}

func TestFind50Primes(t *testing.T) {
	actualPrimes := FindPrime(50)
	expectedPrimes := []uint64{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229}
	AssertEqual(t, actualPrimes, expectedPrimes)
}

func TestFind20Fibonaccies(t *testing.T) {
	actualFibonaccies := FindFibonacci(20)
	expectedFibonaccies := []*big.Int{
		big.NewInt(int64(0)),
		big.NewInt(int64(1)),
		big.NewInt(int64(1)),
		big.NewInt(int64(2)),
		big.NewInt(int64(3)),
		big.NewInt(int64(5)),
		big.NewInt(int64(8)),
		big.NewInt(int64(13)),
		big.NewInt(int64(21)),
		big.NewInt(int64(34)),
		big.NewInt(int64(55)),
		big.NewInt(int64(89)),
		big.NewInt(int64(144)),
		big.NewInt(int64(233)),
		big.NewInt(int64(377)),
		big.NewInt(int64(610)),
		big.NewInt(int64(987)),
		big.NewInt(int64(1597)),
		big.NewInt(int64(2584)),
		big.NewInt(int64(4181)),
	}
	AssertEqual(t, actualFibonaccies, expectedFibonaccies)
}
