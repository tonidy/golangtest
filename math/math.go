package mathx

import (
	"log"
	"math/big"
	"strconv"
	"strings"

	"github.com/JohnCGriffin/overflow"
)

type mathCommand interface {
	sum([]string) *big.Int
	multiply([]string) *big.Int
}

type bigIntCommand struct{}

func (b *bigIntCommand) sum(inputs []string) *big.Int {
	operands := []*big.Int{}
	for _, input := range inputs {
		operand, _ := new(big.Int).SetString(input, 10)
		operands = append(operands, operand)
	}
	total := big.NewInt(0)
	for _, operand := range operands {
		total.Add(total, operand)
	}
	return total
}

func (b *bigIntCommand) multiply(inputs []string) *big.Int {
	operands := []*big.Int{}
	for _, input := range inputs {
		operand, _ := new(big.Int).SetString(input, 10)
		operands = append(operands, operand)
	}
	total := big.NewInt(1)
	for _, operand := range operands {
		total.Mul(total, operand)
	}
	return total
}

type uint64Command struct{}

func (u *uint64Command) sum(inputs []string) *big.Int {
	operands := []uint64{}
	for _, input := range inputs {
		operand, _ := strconv.ParseUint(input, 10, 64)
		operands = append(operands, operand)
	}
	total := uint64(0)
	for _, operand := range operands {
		total += operand
	}
	return new(big.Int).SetUint64(total)
}

func (u *uint64Command) multiply(inputs []string) *big.Int {
	operands := []uint64{}
	for _, input := range inputs {
		operand, _ := strconv.ParseUint(input, 10, 64)
		operands = append(operands, operand)
	}
	total := uint64(1)
	for _, operand := range operands {
		total *= operand
	}
	return new(big.Int).SetUint64(total)
}

//Sum is integer addition
func Sum(inputs []string) *big.Int {
	var command mathCommand = &uint64Command{}

	for _, input := range inputs {
		_, err := strconv.ParseUint(strings.TrimSpace(input), 10, 64)
		if err != nil {
			switch e := err.(type) {
			case *strconv.NumError:
				command := &bigIntCommand{}
				return command.sum(inputs)
			default:
				log.Println(e)
			}
		}
	}
	return command.sum(inputs)
}

//Multiply is integer multiplication
func Multiply(inputs []string) *big.Int {
	var command mathCommand = &uint64Command{}
	tempResult := 1

	for _, input := range inputs {
		result, err := strconv.ParseUint(strings.TrimSpace(input), 10, 64)
		multiplyRes, ok := overflow.Mul(tempResult, int(result))
		tempResult = multiplyRes
		if err != nil {
			switch e := err.(type) {
			case *strconv.NumError:
				command := &bigIntCommand{}
				return command.multiply(inputs)
			default:
				log.Println(e)
			}
		} else if !ok && tempResult <= 1 {
			command := &bigIntCommand{}
			return command.multiply(inputs)
		}
	}
	return command.multiply(inputs)
}

// Copy from: https://golang.org/doc/play/sieve.go
// Send the sequence 2, 3, 4, ... to channel 'ch'.
func generate(ch chan<- uint64) {
	for i := 2; ; i++ {
		ch <- uint64(i) // Send 'i' to channel 'ch'.
	}
}

// Copy the values from channel 'in' to channel 'out',
// removing those divisible by 'prime'.
func filter(in <-chan uint64, out chan<- uint64, prime uint64) {
	for {
		i := <-in // Receive value from 'in'.
		if i%prime != 0 {
			out <- i // Send 'i' to 'out'.
		}
	}
}

//FindPrime is find the first N prime numbers
func FindPrime(n int) []uint64 {
	ch := make(chan uint64) // Create a new channel.
	primes := []uint64{}
	go generate(ch) // Launch Generate goroutine.
	for i := 0; i < n; i++ {
		prime := <-ch
		primes = append(primes, prime)
		ch1 := make(chan uint64)
		go filter(ch, ch1, prime)
		ch = ch1
	}

	return primes
}

//Copy from https://blog.abelotech.com/posts/fibonacci-numbers-golang/
func fibonacciBig(n uint64) *big.Int {
	if n <= 1 {
		return big.NewInt(int64(n))
	}

	var n2, n1 = big.NewInt(0), big.NewInt(1)

	for i := uint64(1); i < n; i++ {
		n2.Add(n2, n1)
		n1, n2 = n2, n1
	}

	return n1
}

//FindFibonacci is find the first N fibonacci sequences
func FindFibonacci(n uint64) []*big.Int {
	fibonaccies := []*big.Int{}

	for i := uint64(0); i < n; i++ {
		fibonaccies = append(fibonaccies, fibonacciBig(i))
	}

	return fibonaccies
}
